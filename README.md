# ChamCLI
A litte tool with no dependancies to configure your [ChameleonUltra](https://github.com/RfidResearchGroup/ChameleonUltra).

This is not a replacement for the official Python client or GameTec-live GUI application. The purpose of this tool is simply to allow the configuration of the ChameleonUltra, not to serve as an interface for the detection, attack or reading functions. I wrote this tool for my personal use, to have a real command line alternative and avoid having to use an interactive client. With `chamcli` you can list the slots and their configuration, adjust the slot configuration parameters, load tag images (MIFARE) in flash, save the contents of an emulated flash in a file, activate/deactivate features of the MIFARE emulator...

Tested on Debian GNU/Linux 12 (amd64), FreeBSD 13 (amd64), NetBSD 9.3 (amd64) and OpenBSD 7.3 (amd64 + macppc). Note that with FreeBSD you must use the `/dev/cuaU`_x_ device. Same under OpenBSD and `/dev/dtyU`_x_ with NetBSD.

Everything is not yet implemented but it is already usable. Examples:

- List slots:

```
$ ./chamcli -d /dev/ttyACM0 -l
Slots status:
  slot 0:
    HF (enabled):  "lav crack"  - Mifare Classic 1k (1001)
       MIFARE emulator settings:  write:Normal  detect:OFF  Gen1A:OFF  Gen2:OFF  block0anticol:ON
    LF (enabled):  "Empty"  - Unknown (0)

  slot 1:
    HF (disabled): "Empty"  - Unknown (0)
    LF (disabled): "Empty"  - Unknown (0)

  slot 2:
    HF (disabled): "Empty"  - Unknown (0)
    LF (disabled): "Empty"  - Unknown (0)

  slot 3:
    HF (disabled): "Empty"  - Unknown (0)
    LF (disabled): "Empty"  - Unknown (0)

  slot 4:
    HF (disabled): "Empty"  - Unknown (0)
    LF (disabled): "Empty"  - Unknown (0)

  slot 5:
    HF (disabled): "Empty"  - Unknown (0)
    LF (disabled): "Empty"  - Unknown (0)

  slot 6:
    HF (disabled): "Empty"  - Unknown (0)
    LF (disabled): "Empty"  - Unknown (0)

* slot 7:
    HF (enabled):  "une espace"  - Mifare Classic 1k (1001)
       MIFARE emulator settings:  write:Normal  detect:OFF  Gen1A:OFF  Gen2:OFF  block0anticol:OFF
       UID(4):12B178C5  ATQA:0400  SAK:08  ATS(0):
    LF (enabled):  "Empty"  - Unknown (0)
```

- Enable and activate slot 6, with Mifare Classic 1k type, loading `lav.bin` image to flash, enabling UID/BCC/SAK/ATQA from block 0 and setting a nickame, in one go:

```
$ ./chamcli -d /dev/ttyACM0 -s 6 -t 1001 -e 1 -a --hf -n "Simple test" -w ~/RFID/lav.bin --blk0 1

$ ./chamcli -d /dev/ttyACM0 -l
[...]
* slot 6:
    HF (enabled):  "Simple test"  - Mifare Classic 1k (1001)
       MIFARE emulator settings:  write:Normal  detect:OFF  Gen1A:OFF  Gen2:OFF  block0anticol:ON
    LF (enabled):  "Empty"  - Unknown (0)
[...]
```

- Disabling UID/BCC/SAK/ATQA from block 0 and setting anticollision data for slot 6:

```
$ ./chamcli -d /dev/ttyACM0 -s 6 --blk0 0 --anticoll 080400deadbeef

$ ./chamcli -d /dev/ttyACM0 -l
[...]
* slot 6:
    HF (enabled):  "Simple test"  - Mifare Classic 1k (1001)
       MIFARE emulator settings:  write:Normal  detect:OFF  Gen1A:OFF  Gen2:OFF  block0anticol:OFF
       UID(4):DEADBEEF  ATQA:0400  SAK:08  ATS(0):
    LF (enabled):  "Empty"  - Unknown (0)
[...]
```

- Use default Mifare Mini data content for slot 4 and enable it (and display slots):

```
$ ./chamcli -d /dev/ttyACM0 -s 4 -t 1000 -i -e 1 --hf -l
[...]
  slot 4:
    HF (enabled):  "Empty"  - Mifare Mini (1000)
       MIFARE emulator settings:  write:Normal  detect:OFF  Gen1A:OFF  Gen2:OFF  block0anticol:OFF
       UID(4):DEADBEEF  ATQA:0400  SAK:08  ATS(0):
    LF (enabled):  "Empty"  - Unknown (0)
[...]
```

- Clear everything for slot 6 (the firmware disables the slot automatically):

```
$ ./chamcli -d /dev/ttyACM0 -s 6 -c -n "Empty" --hf --lf
```

## TODO
- add an option to confirm overwriting an image file (`-y`) when using `-r`
- support `bin` and `eml` files (but not JSON, I don't like JSON)
- show more detailed information when using `-s` with `-l`
- manage global settings (buttons usage, led animation, etc)
- manage BLE stuff (change PIN, clear pairing, etc)
- add factory reset option with confirmation


