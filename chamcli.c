#include <err.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>

#include <sys/errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/mman.h>

#include <arpa/inet.h>

#include <termios.h>

#include "chamcli.h"
#include "chameleon_cmd.h"
#include "chameleon_status.h"
#include "util.h"
#include "color.h"

int optverb;

int
main(int argc, char **argv) {
	int i;
	char *endptr;
	int retopt;
	int fd;
	char *serialdev = NULL;

	int res;

	int opt = 0;
	int optdispslot = 0;
	int optbatt = 0;
	int optversion = 0;
	int optblei = 0;
	int optchange = 0;
	int optinitslot = 0;
	int optcleartype = 0;
	char *optmfrfile = NULL;
	char *optmfwfile = NULL;
	static int optlf = 0;
	static int opthf = 0;

	struct termios oldtio, newtio;

	static struct option long_options[] = {
		{"help",      no_argument,       NULL,         'h' },
		{"verbose",   no_argument,       &optverb,       1 },
		{"lf",        no_argument,       &optlf,         1 },
		{"hf",        no_argument,       &opthf,         1 },
		{"device",    required_argument, NULL,         'd' },
		{"list",      no_argument,       NULL,         'l' },
		{"battery",   no_argument,       NULL,         'b' },
		{"version",   no_argument,       NULL,         'v' },
		{"bleinfo",   no_argument,       NULL,         'k' },
		{"slot",      required_argument, NULL,         's' },
		{"cleartype", no_argument,       NULL,         'c' },
		{"type",      required_argument, NULL,         't' },
		{"enable",    required_argument, NULL,         'e' },
		{"setname",   required_argument, NULL,         'n' },
		{"activate",  no_argument,       NULL,         'a' },
		{"initslot",  no_argument,       NULL,         'i' },
		{"mfread",    required_argument, NULL,         'r' },
		{"mfwrite",   required_argument, NULL,         'w' },
		{"writemode", required_argument, NULL,    OPTWMODE },
		{"detect",    required_argument, NULL,   OPTDETECT },
		{"gen1a",     required_argument, NULL,    OPTGEN1A },
		{"gen2",      required_argument, NULL,     OPTGEN2 },
		{"blk0",      required_argument, NULL,     OPTBLK0 },
		{"anticoll",  required_argument, NULL, OPTANTICOLL },
		{0, 0, 0, 0}
	};
	int option_index = 0;

	uint16_t voltage;
	uint8_t percentage;
	uint8_t enabledmap[16] = { 0 };
	int activeslot = -1;
	uint16_t typemap[16] = { 0 };
	char name[512] = { 0 };
	uint8_t mfsettings[5] = { 0 };
	char gitversion[512] = { 0 };
	uint16_t fwversion = 0;
	char blekey[512] = { 0 };

	char *nickname = NULL;
	uint8_t optslotnum = 0xff;
	uint8_t optmfwmode = 0xff;
	uint8_t optmfgen1a = 0xff;
	uint8_t optmfgen2 = 0xff;
	uint8_t optmfdetect = 0xff;
	uint8_t optmfblk0 = 0xff;
	uint16_t opttype = 0;
	uint8_t optenable = 0xff;
	bool dirtyslot = false;

	int optanticoll = 0;  // 7 if 4 bytes UID - 10 if 7 bytes UID
	uint8_t anticoll_data[10] = { 0x00 };
	uint8_t anticoll_rdata[128] = { 0x00 };
	size_t anticoll_rlen = 0;

	struct stat file_status;
	int mffd;
	unsigned char *mfmmap = NULL;
	size_t mf_bs; // block size
	int mf_nb; // number of blocks

	while ((retopt = getopt_long(argc, argv, "hd:n:s:t:r:w:e:aiclbvk", long_options, &option_index)) != -1) {
		switch (retopt) {
		case 0:
			// If this option set a flag, do nothing else now
			if (long_options[option_index].flag != 0)
				break;
		case 'd':
			serialdev = strdup(optarg);
			break;
		case 'n':
			if (strlen(optarg) > MAXNICKSZ)
				errx(EXIT_FAILURE, "Nickname si too big (UTF-8 encoded string of max 32 bytes only)!");
			nickname = strdup(optarg);
			opt++;
			break;
		case 's':
			if (strlen(optarg) == 1 && optarg[0] >= 48 && optarg[0] <= 55) {
				optslotnum = optarg[0] - 48;
			} else {
				errx(EXIT_FAILURE, "Invalid slot number!");
			}
			break;
		case 't':
			opttype = (uint16_t)strtol(optarg, &endptr, 10);
			if (endptr == optarg || (opttype != TYPE_EM410X && opttype != TYPE_MIFARE_Mini && opttype != TYPE_MIFARE_1024 && opttype != TYPE_MIFARE_2048
						&& opttype != TYPE_MIFARE_4096 && opttype != TYPE_NTAG_213 && opttype != TYPE_NTAG_215 && opttype != TYPE_NTAG_216) ) {
				errx(EXIT_FAILURE, "Invalid tag type!");
			}
			opt++;
			break;
		case 'r':
			optmfrfile = strdup(optarg);
			opt++;
			break;
		case 'w':
			optmfwfile = strdup(optarg);
			opt++;
			break;
		case 'a':
			optchange = 1;
			opt++;
			break;
		case 'i':
			optinitslot = 1;
			opt++;
			break;
		case 'c':
			optcleartype = 1;
			opt++;
			break;
		case 'l':
			optdispslot = 1;
			opt++;
			break;
		case 'b':
			optbatt = 1;
			opt++;
			break;
		case 'k':
			optblei = 1;
			opt++;
			break;
		case 'v':
			optversion = 1;
			opt++;
			break;
		case 'h':
			printhelp(argv[0]);
			return(EXIT_SUCCESS);
		case 'e':
			if (strlen(optarg) == 1 && (optarg[0] == 48 || optarg[0] == 49)) {
				optenable = (uint8_t)(optarg[0] - 48);
				opt++;
			} else {
				errx(EXIT_FAILURE, "Invalid boolean");
			}
			break;
		case OPTDETECT:
			if (strlen(optarg) == 1 && (optarg[0] == 48 || optarg[0] == 49)) {
				optmfdetect = (uint8_t)(optarg[0] - 48);
				opt++;
			} else {
				errx(EXIT_FAILURE, "Invalid boolean");
			}
			break;
		case OPTGEN1A:
			if (strlen(optarg) == 1 && (optarg[0] == 48 || optarg[0] == 49)) {
				optmfgen1a = (uint8_t)(optarg[0] - 48);
				opt++;
			} else {
				errx(EXIT_FAILURE, "Invalid boolean");
			}
			break;
		case OPTGEN2:
			if (strlen(optarg) == 1 && (optarg[0] == 48 || optarg[0] == 49)) {
				optmfgen2 = (uint8_t)(optarg[0] - 48);
				opt++;
			} else {
				errx(EXIT_FAILURE, "Invalid boolean");
			}
			break;
		case OPTBLK0:
			if (strlen(optarg) == 1 && (optarg[0] == 48 || optarg[0] == 49)) {
				optmfblk0 = (uint8_t)(optarg[0] - 48);
				opt++;
			} else {
				errx(EXIT_FAILURE, "Invalid boolean");
			}
			break;
		case OPTWMODE:
			if (strlen(optarg) == 1 && optarg[0] >= 48 && optarg[0] <= 51) {
				optmfwmode = (uint8_t)(optarg[0] - 48);
				opt++;
			} else {
				errx(EXIT_FAILURE, "Invalid MIFARE emulator write mode");
			}
			break;
		case OPTANTICOLL:
			if (strlen(optarg) != 14 && strlen(optarg) != 20)
				errx(EXIT_FAILURE, "Invalid anticollision argument. Must be 7 or 10 hex values.");

			res = hex2array(optarg, anticoll_data, 10);
			if (res != 7 && res != 10)
				errx(EXIT_FAILURE, "Invalid anticollision argument. Must be 7 or 10 hex values.");

			optanticoll = res;
			opt++;
			break;
		default:
			printhelp(argv[0]);
			return(EXIT_FAILURE);
		}
	}

	if (!opt) {
		errx(EXIT_FAILURE, "Nothing to do.\nSee -h or --help for usage.\n");
	}

	if (nickname && (!opthf && !optlf)) {
		free(nickname);
		errx(EXIT_FAILURE, "Error. You must use --lf and/or --hf with --setname.");
	}

	if (nickname && optslotnum > 7) {
		free(nickname);
		errx(EXIT_FAILURE, "Error. You must choose a slot number with --slot or -s.");
	}

	if (optchange && optslotnum > 7) {
		errx(EXIT_FAILURE, "Error. You must choose a slot number with --slot or -s.");
	}

	if (opttype && optslotnum > 7) {
		errx(EXIT_FAILURE, "Error. You must choose slot number with --slot or -s.");
	}

	if ((optmfdetect < 0xff || optmfgen1a < 0xff || optmfgen2 < 0xff || optmfblk0 < 0xff ||
				optmfwmode < 0xff || optenable < 0xff || optanticoll) && optslotnum > 7) {
		errx(EXIT_FAILURE, "Error. You must give a slot number (-s or --slot) to change this setting.");
	}

	if (optenable < 0xff && (!opthf && !optlf)) {
		errx(EXIT_FAILURE, "Error. You must use --lf and/or --hf with --enable.");
	}

	if (optinitslot && (!opttype || optslotnum > 7)) {
		errx(EXIT_FAILURE, "Error. You must choose a slot number with --slot or -s. And a slot type with -t.");
	}

	if (optcleartype && ((!opthf && !optlf) || optslotnum > 7)) {
		errx(EXIT_FAILURE, "Error. You must choose a slot number with --slot or -s. And must use --lf and/or --hf.");
	}

	if (optmfwfile && optmfrfile) {
		if (optmfwfile) free(optmfwfile);
		if (optmfrfile) free(optmfrfile);
		errx(EXIT_FAILURE, "Error. You cannot read and write at the same time.");
	}

	if ((optmfwfile || optmfrfile) && optslotnum > 7) {
		if (optmfwfile) free(optmfwfile);
		if (optmfrfile) free(optmfrfile);
		errx(EXIT_FAILURE, "Error. You must choose a slot number with --slot or -s.");
	}

	/*
	 * Check MIFARE image file
	 * TODO: create -y option to overwrite if destination file exist
	 */
	if (optmfwfile) {
		if (stat(optmfwfile, &file_status) == -1 )
			err(EXIT_FAILURE, "Error getting stats from MIFARE image file");
		if ((mffd = open(optmfwfile, O_RDONLY)) == -1)
			err(EXIT_FAILURE, "Error opening MIFARE image file for reading");
	}

	if (optmfrfile) {
		if ((mffd = open(optmfrfile, O_RDWR | O_CREAT, (mode_t)0644)) == -1)
			err(EXIT_FAILURE, "Error opening MIFARE image file for writing");
	}


	if (!serialdev) {
		errx(EXIT_FAILURE, "Error. You must give me a device file to use with -d or --device.");
	}

	if ((fd = open(serialdev, O_RDWR | O_NOCTTY)) < 0) {
		err(EXIT_FAILURE, "Unable to open serial port");
	} else if (optverb) {
		printf("%s open ok\n", serialdev);
	}

	// save current termios configuration
	if(tcgetattr(fd, &oldtio) != 0) {
		fprintf(stderr,"tcgetattr Error : %s (%d)\n",
				strerror(errno),errno);
		close(fd);
		exit(EXIT_FAILURE);
	} else if (optverb) {
		printf("tcgetattr ok\n");
	}

	// the new termios configuration is based on the old one
	newtio = oldtio;

	/* serial configuration */
	newtio.c_oflag &= ~OPOST;
	newtio.c_cflag &= ~(CRTSCTS | PARENB | CSIZE | ECHO);
	newtio.c_cflag |= (BAUDRATE | CREAD | CS8 | CLOCAL);
	newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG | IEXTEN);
	newtio.c_iflag &= ~(IXON | IXOFF | IXANY | ICRNL | IGNCR | INLCR);
	newtio.c_cc[VTIME] = 5;
	newtio.c_cc[VMIN] = 0;

	// flush everything
	if (tcflush(fd, TCIOFLUSH) != 0) {
		fprintf(stderr,"tcflush Error : %s (%d)\n",
				strerror(errno),errno);
		close(fd);
		exit(EXIT_FAILURE);
	} else if (optverb) {
		printf("tcflush ok\n");
	}

	// set config
	if (tcsetattr(fd,TCSANOW,&newtio) != 0) {
		fprintf(stderr,"tcsetattr Error : %s (%d)\n",
				strerror(errno),errno);
		close(fd);
		exit(EXIT_FAILURE);
	} else if (optverb) {
		printf("tcsetattr newtio ok\n");
	}

	/*
	 * Show battery information
	 */
	if (optbatt) {
		if (cmd_get_battery(fd, &voltage, &percentage) < 0) {
			warnx("Error getting battery status!\n");
		} else {
			printf("Battery status:\n");
			printf("  voltage:    %umV\n", voltage);
			printf("  percentage: %u%%\n", percentage);
		}
		printf("\n");
	}

	/*
	 * Show firwmare app and git versions
	 */
	if (optversion) {
		if (cmd_get_fwversion(fd, &fwversion) < 0) {
			warnx("Error getting firmware version from device!\n");
		} else if (cmd_get_gitversion(fd, gitversion) < 0) {
			warnx("Error getting git version from device!\n");
		} else {
			printf("Version %u.%u (%s)\n", fwversion/256, fwversion % 256, gitversion);
		}
		printf("\n");
	}

	/*
	 * Show BLE connect key
	 */
	if (optblei) {
		printf("BLE configuration:\n");
		if ((res = cmd_get_bleenable(fd)) < 0) {
			warnx("Error getting BLE info from device!\n");
		} else {
			printf("  BLE pairing is %s " RESET " \n", res ? BOLDGREEN "enabled" : RED "disabled" );
		}
		if (cmd_get_blekey(fd, blekey) < 0) {
			warnx("Error getting BLE connect key from device!\n");
		} else {
			printf("  BLE pairing key: %s\n", blekey);
		}
		printf("\n");
	}

	/*
	 * Change slot nickname
	 */
	if (nickname) {
		if (optlf) {
			if (cmd_set_slotnick(fd, optslotnum, TAG_SENSE_LF, nickname, strlen(nickname)) < 0) {
				warnx("Error setting nickname for slot %u (LF)!\n", optslotnum);
			} else {
				dirtyslot = true;
			}
		}
		if (opthf) {
			if (cmd_set_slotnick(fd, optslotnum, TAG_SENSE_HF, nickname, strlen(nickname)) < 0) {
				warnx("Error setting nickname for slot %u (HF)!\n", optslotnum);
			} else {
				dirtyslot = true;
			}
		}
	}

	/*
	 * Change active slot
	 */
	if (optchange) {
		if (cmd_set_activeslot(fd, optslotnum) < 0) {
			warnx("Error activating slot!\n");
		}
	}

	/*
	 * Change MIFARE emulator settings
	 */
	if (optmfdetect < 0xff || optmfgen1a < 0xff || optmfgen2 < 0xff || optmfblk0 < 0xff || optmfwmode < 0xff) {
		// save active slot number
		if ((activeslot = cmd_get_activeslot(fd)) < 0) {
			warnx("Error getting active slot number!\n");
		}

		// activate slot number from options
		if (cmd_set_activeslot(fd, optslotnum) < 0) {
			warnx("Error activating slot!\n");
		}

		if (optmfdetect < 0xff) {
			if (cmd_set_mfemulsetdetect(fd, optmfdetect) < 0) {
				warnx("Error setting MIFARE emulator Detection (mfkey32) mode for slot %u!\n", optslotnum);
			} else {
				dirtyslot = true;
			}
		}

		if (optmfgen1a < 0xff) {
			if (cmd_set_mfemulsetgen1a(fd, optmfgen1a) < 0) {
				warnx("Error setting MIFARE emulator Gen1A magic mode for slot %u!\n", optslotnum);
			} else {
				dirtyslot = true;
			}
		}

		if (optmfgen2 < 0xff) {
			if (cmd_set_mfemulsetgen2(fd, optmfgen2) < 0) {
				warnx("Error setting MIFARE emulator Gen2 magic mode for slot %u!\n", optslotnum);
			} else {
				dirtyslot = true;
			}
		}

		if (optmfblk0 < 0xff) {
			if (cmd_set_mfemulsetblk0col(fd, optmfblk0) < 0) {
				warnx("Error setting MIFARE emulator UID/BCC/SAK/ATQA from block 0 mode for slot %u!\n", optslotnum);
			} else {
				dirtyslot = true;
			}
		}

		if (optmfwmode < 0xff) {
			if (cmd_set_mfemulsetwmode(fd, optmfwmode) < 0) {
				warnx("Error setting MIFARE emulator write mode for slot %u!\n", optslotnum);
			} else {
				dirtyslot = true;
			}
		}

		// restore initial active slot state
		if (cmd_set_activeslot(fd, activeslot) < 0) {
			warnx("Error activating slot!\n");
		}
	}

	/*
	 * Change slot type (and initialize with default data)
	 */
	if (opttype) {
		if (optinitslot) {
			if (cmd_initslot(fd, optslotnum, opttype) < 0) {
				warnx("Error initialising slot with type %u default data!\n", opttype);
			}
		}
		if (cmd_set_slottype(fd, optslotnum, opttype) < 0) {
			warnx("Error changing slot type!\n");
		} else {
			dirtyslot = true;
		}
	}

	/*
	 * clear slot type
	 */
	if (optcleartype) {
		if (optlf) {
			if (cmd_clear_slottype(fd, optslotnum, TAG_SENSE_LF) <0) {
				warnx("Error clearing slot type (LF)!\n");
			} else {
				dirtyslot = true;
			}
		}
		if (opthf) {
			if (cmd_clear_slottype(fd, optslotnum, TAG_SENSE_HF) <0) {
				warnx("Error clearing slot type (HF)!\n");
			} else {
				dirtyslot = true;
			}
		}
	}


	/*
	 * Load MIFARE image from file to emulator flash
	 */
	if (optmfwfile) {
		if (cmd_get_slotsinfo(fd, typemap) < 0) {
			warnx("Error getting list of slot types!\n");
		} else {
			switch(typemap[optslotnum * 2]) {
			case TYPE_MIFARE_Mini:
				mf_bs = 160; mf_nb = 2; break;
			case TYPE_MIFARE_1024:
				mf_bs = 128; mf_nb = 8; break;
			case TYPE_MIFARE_2048:
				mf_bs = 128; mf_nb = 16; break;
			case TYPE_MIFARE_4096:
				mf_bs = 128; mf_nb = 32; break;
			default:
				mf_bs = 0; mf_nb = 0;
			}
			if (!mf_bs || !mf_nb) {
				warnx("Image loading only works for Mifare Mini, Mifare Classic 1k, Mifare Classic 2k or Mifare Classic 4k slots!\n");
			} else if (file_status.st_size != (mf_bs * mf_nb)) {
				warnx("Invalid file size (%zu bytes). This slot needs exactly %zu bytes!\n", file_status.st_size, mf_bs * mf_nb);
			} else {
				if ((mfmmap = mmap(0, file_status.st_size, PROT_READ, MAP_SHARED, mffd, 0)) == MAP_FAILED) {
					close(mffd);
					err(EXIT_FAILURE, "Error mmapping MIFARE image file");
				}

				// backup active slot
				if ((activeslot = cmd_get_activeslot(fd)) < 0) {
					warnx("Error getting active slot number!\n");
				}

				// activate slot number from options
				if (cmd_set_activeslot(fd, optslotnum) < 0) {
					warnx("Error activating slot!\n");
				}

				for (i = 0; i < mf_nb; i++) {
					if (cmd_loadmfblock(fd, i * 8, mfmmap + (i * mf_bs), mf_bs) < 0) {
						warnx("Error writing MIFARE image to flash!\n");
					}
				}

				if (munmap(mfmmap, file_status.st_size) == -1)
					warnx("Error un-mmapping MIFARE image file");

				// restore initial active slot state
				if (cmd_set_activeslot(fd, activeslot) < 0) {
					warnx("Error activating slot!\n");
				}
			}
		}
	}

	/*
	 * Save MIFARE image from emulator flash to file
	 */
	if (optmfrfile) {
		if (cmd_get_slotsinfo(fd, typemap) < 0) {
			warnx("Error getting list of slot types!\n");
		} else {
			switch(typemap[optslotnum * 2]) {
			case TYPE_MIFARE_Mini:
				mf_bs = 80; mf_nb = 4; break;
			case TYPE_MIFARE_1024:
				mf_bs = 64; mf_nb = 16; break;
			case TYPE_MIFARE_2048:
				mf_bs = 64; mf_nb = 32; break;
			case TYPE_MIFARE_4096:
				mf_bs = 64; mf_nb = 64; break;
			default:
				mf_bs = 0; mf_nb = 0;
			}
			if (!mf_bs || !mf_nb) {
				warnx("Image loading only works for Mifare Mini, Mifare Classic 1k, Mifare Classic 2k or Mifare Classic 4k slots!\n");
			} else {
				if (lseek(mffd, (mf_nb * mf_bs) - 1, SEEK_SET) == -1) {
					tcsetattr(fd,TCSANOW,&oldtio);
					close(fd);
					close(mffd);
					err(EXIT_FAILURE, "Error stretching output file (lseek)!\n");
				}
				if (write(mffd, "", 1) == -1) {
					tcsetattr(fd,TCSANOW,&oldtio);
					close(fd);
					close(mffd);
					err(EXIT_FAILURE, "Error stretching output file (write)!\n");
				}
				if ((mfmmap = mmap(0, mf_nb * mf_bs, PROT_READ | PROT_WRITE, MAP_SHARED, mffd, 0)) == MAP_FAILED) {
					// restore previous serial config
					tcsetattr(fd,TCSANOW,&oldtio);
					close(fd);
					close(mffd);
					err(EXIT_FAILURE, "Error mmapping MIFARE image file");
				}

				// backup active slot
				if ((activeslot = cmd_get_activeslot(fd)) < 0) {
					warnx("Error getting active slot number!\n");
				}

				// activate slot number from options
				if (cmd_set_activeslot(fd, optslotnum) < 0) {
					warnx("Error activating slot!\n");
				}

				// serial FIFO is 128 bytes, we cannot read more
				for (i = 0; i < mf_nb; i++) {
					if (cmd_readmfblock(fd, i * 4, mf_bs / 16, mfmmap + (i * mf_bs)) < 0) {
						warnx("Error reading MIFARE image from flash!\n");
					}
				}

				msync(mfmmap, mf_nb * mf_bs, MS_SYNC);

				if (munmap(mfmmap, mf_nb * mf_bs) == -1)
					warnx("Error un-mmapping MIFARE image file");

				// restore initial active slot state
				if (cmd_set_activeslot(fd, activeslot) < 0) {
					warnx("Error activating slot!\n");
				}
			}
		}
	}

	/*
	 * Enable/disable slot
	 */
	if (optenable < 0xff) {
		if (opthf) {
			if (cmd_set_slotenable(fd, optslotnum, TAG_SENSE_HF, optenable) < 0) {
				warnx("Error %s slot %u!\n", optenable ? "enabling" : "disabling", optslotnum);
			}
		}
		if (optlf) {
			if (cmd_set_slotenable(fd, optslotnum, TAG_SENSE_LF, optenable) < 0) {
				warnx("Error %s slot %u!\n", optenable ? "enabling" : "disabling", optslotnum);
			}
		}
	}

	/*
	 * set anticollision data (UID + SAK + ATQA)
	 */
	if (optanticoll) {
		// backup active slot
		if ((activeslot = cmd_get_activeslot(fd)) < 0) {
			warnx("Error getting active slot number!\n");
		}

		// activate slot number from options
		if (cmd_set_activeslot(fd, optslotnum) < 0) {
			warnx("Error activating slot!\n");
		}

		if (cmd_set_anticoll(fd, anticoll_data, optanticoll) < 0) {
			warnx("Error setting anticollision data!\n");
		}

		// restore initial active slot state
		if (cmd_set_activeslot(fd, activeslot) < 0) {
			warnx("Error activating slot!\n");
		}
	}

	/*
	 * slot configuration need to be saved
	 */
	if (dirtyslot) {
		if (cmd_save_config(fd) <0) {
			warnx("Error saving configuration in flash!\n");
		}
	}

	/*
	 * Show slots information
	 */
	if (optdispslot) {
		if (cmd_get_enabledslots(fd, enabledmap) < 0) {
			warnx("Error getting list of enabled slots!\n");
		}

		// backup active slot
		if ((activeslot = cmd_get_activeslot(fd)) < 0) {
			warnx("Error getting active slot number!\n");
		}

		if (cmd_get_slotsinfo(fd, typemap) < 0) {
			warnx("Error getting list of slot types!\n");
		}

		printf("Slots status:\n");
		for (i = 0; i < 8; i++) {
			printf("%s %sslot %d" RESET ":\n", activeslot == i ? BOLDYELLOW "*" RESET: " ", enabledmap[i * 2] || enabledmap[(i * 2) + 1] ? BOLDGREEN : "", i);

			if (cmd_set_activeslot(fd, (uint8_t)i) < 0) {
				warnx("Error activating slot!\n");
			}

			// HF part
			if (cmd_get_slotnick(fd, i, TAG_SENSE_HF, name) < 0) {
				printf("    %sHF" RESET " %s N/A  - ", enabledmap[i * 2] ? GREEN : RED, enabledmap[i * 2] ? "(enabled): " : "(disabled):");
			} else {
				printf("    %sHF" RESET " %s " YELLOW  "\"%s\"" RESET "  - ", enabledmap[i * 2] ? GREEN : RED, enabledmap[i * 2] ? "(enabled): " : "(disabled):", name);
			}
			printtagtype(typemap[i * 2]);
			printf(" (%u)", typemap[i * 2]);
			if (typemap[i * 2] == TYPE_MIFARE_Mini || typemap[i * 2] == TYPE_MIFARE_1024 || typemap[i * 2] == TYPE_MIFARE_2048 || typemap[i * 2] == TYPE_MIFARE_4096) {
				// This is a MIFARE emulation slot, we display emulator settings
				// activate slot
				if (cmd_get_mfemulsettings(fd, mfsettings) < 0) {
					warnx("Error getting Mifare Classic emulator settings!\n");
				} else {
					printf("\n       ");
					printmfemulsettingsline(mfsettings);
				}
			}
			if (typemap[i * 2] != 0 && typemap[i * 2] != 1) {
				if (!mfsettings[3]) {
					if (cmd_get_anticoll(fd, anticoll_rdata, &anticoll_rlen) < 0) {
						warnx("Error getting anticol data!\n");
					} else {
						printf("\n       ");
						printanticolldata(anticoll_rdata, anticoll_rlen);
					}
				}
			}
			printf("\n");

			// LF part
			if (cmd_get_slotnick(fd, i, TAG_SENSE_LF, name) < 0) {
				//printf("    nick(LF): N/A  - ");
				printf("    %sLF" RESET " %s N/A  - ", enabledmap[(i * 2) + 1] ? GREEN : RED, enabledmap[i * 2] ? "(enabled): " : "(disabled):");
			} else {
				//printf("    nick(LF): " YELLOW "\"%s\"" RESET "  - ", name);
				printf("    %sLF" RESET " %s " YELLOW  "\"%s\"" RESET "  - ", enabledmap[(i * 2) + 1] ? GREEN : RED, enabledmap[i * 2] ? "(enabled): " : "(disabled):", name);
			}
			printtagtype(typemap[(i * 2) + 1]);
			printf(" (%u)", typemap[(i * 2) + 1]);
			printf("\n\n");
		}

		// restore initial active slot state
		if (cmd_set_activeslot(fd, activeslot) < 0) {
			warnx("Error activating slot!\n");
		}

		printf("\n");
	}

	// restore previous serial config
	tcsetattr(fd,TCSANOW,&oldtio);
	close(fd);

	if (nickname)
		free(nickname);

	if (optmfwfile) {
		free(optmfwfile);
		close(mffd);
	}

	if (optmfrfile) {
		free(optmfrfile);
		close(mffd);
	}

	if (serialdev)
		free(serialdev);

	return(EXIT_SUCCESS);
}


