#pragma once

void printhelp(char *binname);
uint8_t calclrc(uint8_t *data, size_t len);
void printstatus(uint16_t err);
void printtagtype(uint16_t type);
void printmfemulsettings(uint8_t *settings);
void printmfemulsettingsline(uint8_t *settings);
size_t hex2array(const char *line, uint8_t *data, size_t len);
void printanticolldata(uint8_t *data, size_t len);

