#pragma once

#define BAUDRATE    B115200
#define BUFFSIZE        530

#define MAXNICKSZ        32

#define TAG_SENSE_NO      0
#define TAG_SENSE_LF      1
#define TAG_SENSE_HF      2


#define TYPE_UNKNOWN      0
// 125 kHz (id) cards
#define TYPE_EM410X       100
// Mifare Classic
#define TYPE_MIFARE_Mini  1000
#define TYPE_MIFARE_1024  1001
#define TYPE_MIFARE_2048  1002
#define TYPE_MIFARE_4096  1003
// NTAG
#define TYPE_NTAG_213     1100
#define TYPE_NTAG_215     1101
#define TYPE_NTAG_216     1102

#define MFMODE_NORMAL     0
#define MFMODE_DENIED     1
#define MFMODE_DECEIVE    2
#define MFMODE_SHADOW     3
#define MFMODE_MAX        3

#define OPTDETECT       400
#define OPTGEN1A        401
#define OPTGEN2         403
#define OPTBLK0         404
#define OPTWMODE        405
#define OPTANTICOLL     406

#define FROMBIG(x) ntohs(*(uint16_t *)(x))
#define TOBIG(x, y) *((uint16_t *)(x)) = htons(y)

