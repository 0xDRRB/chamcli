#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <err.h>
#include <arpa/inet.h>

#include "chamcli.h"
#include "chameleon_cmd.h"
#include "chameleon_status.h"
#include "util.h"
#include "color.h"

extern int optverb;

/*
 * args : FD, data sent, data sent length, data received, data received len, excpected response code
 */
ssize_t
serxfer(int fd, uint8_t *sdata, uint8_t *rdata, size_t len, uint16_t resp)
{
	int i;

	if (write(fd, sdata, len) != len){
		warn("Serial write Error!");
		return(-1);
	} else if (optverb) {
		printf("> ");
		for (i = 0; i < len; i++) {
			printf("%02x ", sdata[i]);
		}
		printf("\n");
	}

	ssize_t res = 0;

	usleep(100);

	memset(rdata, 0x00, BUFFSIZE);
	res = read(fd, rdata, BUFFSIZE);

	if(res < 0) {
		warn("Serial read Error");
		return(0);
	}

	if(res == 0) {
		fprintf(stderr, "Nothing readed\n");
		return(0);
	}

	if (optverb) {
		printf("%zd bytes readed\n", res);
		printf("< ");
		for (i = 0; i < res; i++) {
			printf("%02x ", rdata[i]);
		}
		printf("\n");
	}

	if (res < 10) {
		printf("Invalid response\n");
		return(0);
	}

	if (calclrc(rdata, 1) != rdata[1] && calclrc(rdata+2, 6) != rdata[8] && calclrc(rdata + 9, (size_t)FROMBIG(rdata + 6)) != rdata[9 + (size_t)FROMBIG(rdata + 6)]) {
		printf("LCR error\n");
		return(0);
	}

	if (FROMBIG(rdata + 4) != resp) {
		if (optverb) {
			printf("Command NOT successful (0x%04x): ", FROMBIG(rdata + 4));
			printstatus(FROMBIG(rdata + 4));
		}
		return(0);
	}

	return(res);
}

/*
 *  SOF LRC1   CMD   STAT   LEN   LRC2    DATA    LRC3
 * [xx] [xx] [xxxx] [xxxx] [xxxx] [xx] [xxxxx...] [xx]
 *
 * SOF: 1 Byte, the "Magic Byte" represent the start of a packet, must be 0x11.
 * LRC1: 1 Byte, the LRC (Longitudinal Redundancy Check) of the SOF, must be 0xEF.
 * CMD: 2 Bytes in unsigned Big Endian format, each command have been assigned a unique number (e.g. factoryReset(1020)), this is what you are sending to the device.
 * STATUS: 2 Bytes in unsigned Big Endian format. If the direction is from APP to hardware, the status is always 0x0000. If the direction is from hardware to APP, the status is the result of the command.
 * LEN: 2 Bytes in unsigned Big Endian format, the length of the data, maximum is 512.
 * LRC2: 1 Byte, the LRC (Longitudinal Redundancy Check) of the CMD, STATUS and LEN.
 * DATA: LEN Bytes, the data to send or receive, maximum is 512 Bytes. This could be anything, for example you should sending key type, block number, and the card keys when reading a block.
 * LRC3: 1 Byte, the LRC (Longitudinal Redundancy Check) of the DATA.
 */

int
cmd_get_battery(int fd, uint16_t *voltage, uint8_t *percentage)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_BATTERY_INFO >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_BATTERY_INFO & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) != 3) {
		if (optverb)
			warnx("Invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	*voltage = FROMBIG(rbuf + 9);
	*percentage = rbuf[11];

	return(0);
}

int
cmd_get_slotnick(int fd, uint8_t slot, uint8_t sensetype, char *nickname)
{
	// st -> SenseType -> HL/LF = 0 no, 1 LF (125 kHz), 2 HF (13.56 MHz)
	// s -> slots -> 0..7
	// Data = [s][st]
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0]  = 0x11; // SOF
	wbuf[1]  = 0x00; // LRC1 (SOF) calculated later
	wbuf[2]  = ((CMD_GET_SLOT_TAG_NICK >> 8) & 0xff); // CMD msb
	wbuf[3]  = CMD_GET_SLOT_TAG_NICK & 0xff; // CMD lsb
	wbuf[4]  = 0x00; // STATUS msb
	wbuf[5]  = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6]  = 0x00; // LEN msb
	wbuf[7]  = 0x02; // LEN lsb
	wbuf[8]  = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9]  = slot; // data : slot number
	wbuf[10] = sensetype; // data : sensetype
	wbuf[11] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 12, STATUS_DEVICE_SUCCESS))
		return(-1);

	// check data len
	if (!FROMBIG(rbuf + 6)) {
		if (optverb)
			warnx("Invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	memcpy(nickname, rbuf + 9, FROMBIG(rbuf + 6));
	nickname[FROMBIG(rbuf + 6)] = '\0';

	return(0);
}

int
cmd_set_slotnick(int fd, uint8_t slot, uint8_t sensetype, char *nickname, size_t nicklen)
{
	// st -> SenseType -> HL/LF = 0 no, 1 LF (125 kHz), 2 HF (13.56 MHz)
	// s -> slots -> 0..7
	// Data = [s][st]
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0]  = 0x11; // SOF
	wbuf[1]  = 0x00; // LRC1 (SOF) calculated later
	wbuf[2]  = ((CMD_SET_SLOT_TAG_NICK >> 8) & 0xff); // CMD msb
	wbuf[3]  = CMD_SET_SLOT_TAG_NICK & 0xff; // CMD lsb
	wbuf[4]  = 0x00; // STATUS msb
	wbuf[5]  = 0x00; // STATUS lsb -> (app->hw = 0x0000)

	TOBIG(wbuf + 6, 2 + nicklen);

	wbuf[8]  = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9]  = slot; // data : slot number
	wbuf[10]  = sensetype; // data : HF nick or LF nick

	memcpy(wbuf + 11, nickname, nicklen);

	wbuf[9 + 2 + nicklen] = sensetype; // data : sensetype
	wbuf[9 + 2 + nicklen + 1 ] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 9 + 2 + nicklen + 1, STATUS_DEVICE_SUCCESS))
		return(-1);

	return(0);
}

int
cmd_get_enabledslots(int fd, uint8_t *map)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];
	int i;

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_ENABLED_SLOTS >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_ENABLED_SLOTS & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) != 16) {
		if (optverb)
			warnx("Invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	for (i = 0; i < 16; i++) {
		map[i] = rbuf[9 + i];
	}

	return(0);
}

int
cmd_get_activeslot(int fd)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_ACTIVE_SLOT >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_ACTIVE_SLOT & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) != 1) {
		if (optverb)
			warnx("Invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	return(rbuf[9]);
}

int
cmd_set_activeslot(int fd, int slotnum)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_SLOT_ACTIVATED >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_SLOT_ACTIVATED & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x01; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] =(uint8_t)slotnum;
	wbuf[10] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 11, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_get_slotsinfo(int fd, uint16_t *typemap)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];
	int i;

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_SLOT_INFO >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_SLOT_INFO & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) != 32) {
		if (optverb)
			warnx("Invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	for (i = 0; i < 16; i++) {
		typemap[i] = FROMBIG((rbuf + 9) + (i * 2));
	}

	return(0);
}

int
cmd_get_mfemulsettings(int fd, uint8_t *settings)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_MF1_EMULATOR_CONFIG >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_MF1_EMULATOR_CONFIG & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) != 5) {
		if (optverb)
			warnx("Invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	memcpy(settings, rbuf + 9, 5);

	return(0);
}

int
cmd_set_mfemulsetwmode(int fd, uint8_t wmode)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_MF1_WRITE_MODE >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_MF1_WRITE_MODE & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x01; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = wmode; // data (mode)
	wbuf[10] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 11, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_set_mfemulsetgen1a(int fd, uint8_t state)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_MF1_GEN1A_MODE >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_MF1_GEN1A_MODE & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x01; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = state; // data
	wbuf[10] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 11, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_set_mfemulsetgen2(int fd, uint8_t state)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_MF1_GEN2_MODE >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_MF1_GEN2_MODE & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x01; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = state; // data
	wbuf[10] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 11, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_set_mfemulsetblk0col(int fd, uint8_t state)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_MF1_USE_FIRST_BLOCK_COLL >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_MF1_USE_FIRST_BLOCK_COLL & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x01; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = state; // data
	wbuf[10] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 11, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_set_mfemulsetdetect(int fd, uint8_t state)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_MF1_DETECTION_ENABLE >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_MF1_DETECTION_ENABLE & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x01; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = state; // data
	wbuf[10] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 11, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_get_fwversion(int fd, uint16_t *ver)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_APP_VERSION >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_APP_VERSION & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// compute LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) != 2) {
		if (optverb)
			warnx("Invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	*ver = FROMBIG(rbuf + 9);

	return(0);
}

int
cmd_get_gitversion(int fd, char *strversion)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_GIT_VERSION >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_GIT_VERSION & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) < 1) {
		if (optverb)
			warnx("Invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	memcpy(strversion, rbuf + 9, FROMBIG(rbuf + 6));
	strversion[FROMBIG(rbuf + 6)] = '\0';

	return(0);
}

int
cmd_get_blekey(int fd, char *key)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_BLE_CONNECT_KEY_CONFIG >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_BLE_CONNECT_KEY_CONFIG & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// compute LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) != 6) {
		if (optverb)
			warnx("Invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	memcpy(key, rbuf + 9, FROMBIG(rbuf + 6));
	key[FROMBIG(rbuf + 6)] = '\0';

	return(0);
}

int
cmd_get_bleenable(int fd)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_BLE_PAIRING_ENABLE >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_BLE_PAIRING_ENABLE & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// compute LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf, (size_t)FROMBIG(wbuf + 6) + 9); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) != 1) {
		if (optverb)
			warnx("invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	if (rbuf[9] == 1)
		return(1);

	return(0);
}

int
cmd_save_config(int fd)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SLOT_DATA_CONFIG_SAVE >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SLOT_DATA_CONFIG_SAVE & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_set_slottype(int fd, uint8_t slot, uint16_t type)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_SLOT_TAG_TYPE >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_SLOT_TAG_TYPE & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x03; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = slot; // data - slot number
	TOBIG(wbuf + 10, type);
	wbuf[12] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 13, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_clear_slottype(int fd, uint8_t slot, uint8_t sensetype)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_DELETE_SLOT_SENSE_TYPE >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_DELETE_SLOT_SENSE_TYPE & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x02; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = slot; // data - slot number
	wbuf[10] = sensetype; // data - HF|LF
	wbuf[11] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 12, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_loadmfblock(int fd, uint8_t offset, uint8_t *data, size_t datalen)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];
	int i;

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_LOAD_MF1_EMU_BLOCK_DATA >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_LOAD_MF1_EMU_BLOCK_DATA & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	TOBIG(wbuf + 6, 1 + datalen);
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = offset; // data[0]

	for (i = 0; i < datalen; i++) {
		wbuf[10 + i] = data[i];
	}

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 9 + (size_t)FROMBIG(wbuf + 6) + 1, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_readmfblock(int fd, uint8_t offset, uint8_t nblock, uint8_t *data)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];
	int i;

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_READ_MF1_EMU_BLOCK_DATA >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_READ_MF1_EMU_BLOCK_DATA & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x02; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = offset; // data - block start
	wbuf[10] = nblock; // data - block count * 16 bytes
	wbuf[11] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 12, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (FROMBIG(rbuf + 6) != nblock * 16) {
		if (optverb)
			warnx("invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	for (i = 0; i < nblock * 16; i++) {
		data[i] = rbuf[9 + i];
	}

	return(0);
}

int
cmd_set_slotenable(int fd, uint8_t slot, uint8_t sensetype, uint8_t enable)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_SLOT_ENABLE >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_SLOT_ENABLE & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x03; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = slot; // data - slot number
	wbuf[10] = sensetype; // data - LF or HF
	wbuf[11] = enable; // data - enable bool
	wbuf[12] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 13, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_set_anticoll(int fd, uint8_t *data, size_t len)
{
	// [SAK(1)][ATQA(2)][UID(4 or 7)] -> uidlen|uid[uidlen]|atqa[2]|sak|atslen|ats[atslen]
	// xx   xx xx   xx xx xx xx          -> 04 uu uu uu uu aa aa ss 00 (9)
	// xx   xx xx   xx xx xx xx xx xx xx -> 07 uu uu uu uu uu uu uu aa aa ss 00 (12)
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_MF1_ANTI_COLLISION_RES >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_MF1_ANTI_COLLISION_RES & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	if (len == 10) { // 7 bytes UID
		wbuf[6] = 0x00; // LEN msb
		wbuf[7] = 0x0c; // LEN lsb
	} else { // 4 bytes UID
		wbuf[6] = 0x00; // LEN msb
		wbuf[7] = 0x09; // LEN lsb
	}
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later

	wbuf[9] = len == 10 ? 0x07 : 0x04;
	wbuf[10] = data[3]; // data - UID:0
	wbuf[11] = data[4]; // data - UID:1
	wbuf[12] = data[5]; // data - UID:2
	wbuf[13] = data[6]; // data - UID:3
	if (len == 10) {
		wbuf[14] = data[7]; // data - UID:4
		wbuf[15] = data[8]; // data - UID:5
		wbuf[16] = data[9]; // data - UID:6
		wbuf[17] = data[1]; // data - ATQA:0
		wbuf[18] = data[2]; // data - ATQA:1
		wbuf[19] = data[0]; // data - SAK
		wbuf[20] = 0x00; // ATS len
		wbuf[21] = 0x00;// LRC3 (DATA) calculated later
	} else {
		wbuf[14] = data[1]; // data - ATQA:0
		wbuf[15] = data[2]; // data - ATQA:1
		wbuf[16] = data[0]; // data - SAK
		wbuf[17] = 0x00; // ATS len
		wbuf[18] = 0x00;// LRC3 (DATA) calculated later
	}

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, len == 10 ? 22 : 19, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_initslot(int fd, uint8_t slot, uint16_t type)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_SET_SLOT_DATA_DEFAULT >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_SET_SLOT_DATA_DEFAULT & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x03; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	wbuf[9] = slot; // data - slot number
	TOBIG(wbuf + 10, type);
	wbuf[12] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 13, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	return(0);
}

int
cmd_get_anticoll(int fd, uint8_t *data, size_t *datalen)
{
	uint8_t rbuf[BUFFSIZE];
	uint8_t wbuf[BUFFSIZE];
	int i;

	memset(wbuf, 0x00, sizeof(wbuf));

	wbuf[0] = 0x11; // SOF
	wbuf[1] = 0x00; // LRC1 (SOF) calculated later
	wbuf[2] = ((CMD_GET_MF1_ANTI_COLL_DATA >> 8) & 0xff); // CMD msb
	wbuf[3] = CMD_GET_MF1_ANTI_COLL_DATA & 0xff; // CMD lsb
	wbuf[4] = 0x00; // STATUS msb
	wbuf[5] = 0x00; // STATUS lsb -> (app->hw = 0x0000)
	wbuf[6] = 0x00; // LEN msb
	wbuf[7] = 0x00; // LEN lsb
	wbuf[8] = 0x00; // LRC2 (CMD, STATUS, LEN) calculated later
	// no data
	wbuf[9] = 0x00; // LRC3 (DATA) calculated later

	// calc LRCs
	wbuf[1] = calclrc(wbuf, 1);
	wbuf[8] = calclrc(wbuf+2, 6);
	wbuf[9 + (size_t)FROMBIG(wbuf + 6)] = calclrc(wbuf + 9, (size_t)FROMBIG(wbuf + 6)); // LRC on data == LRC on all frame

	if (!serxfer(fd, wbuf, rbuf, 10, STATUS_DEVICE_SUCCESS)) {
		return(-1);
	}

	// check data len
	if (!FROMBIG(rbuf + 6)) {
		if (optverb)
			warnx("invalid data length (%u)\n", FROMBIG(rbuf + 6));
		return(-1);
	}

	for (i = 0; i < FROMBIG(rbuf + 6); i++) {
		data[i] = rbuf[9 + i];
	}

	*datalen = FROMBIG(rbuf + 6);

	return(0);
}
