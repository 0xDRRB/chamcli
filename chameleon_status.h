#pragma once

#define HF_TAG_OK                   0x00 // IC card operation is successful
#define HF_TAG_NO                   0x01 // IC card not found
#define HF_ERR_STAT                 0x02 // Abnormal IC card communication
#define HF_ERR_CRC                  0x03 // IC card communication verification abnormal
#define HF_COLLISION                0x04 // IC card conflict
#define HF_ERR_BCC                  0x05 // IC card BCC error
#define MF_ERR_AUTH                 0x06 // MF card verification failed
#define HF_ERR_PARITY               0x07 // IC card parity error

#define DARKSIDE_CANT_FIXED_NT      0x20 // Darkside, the random number cannot be fixed, this situation may appear on the UID card
#define DARKSIDE_LUCK_AUTH_OK       0x21 // Darkside, the direct verification is successful, maybe the key is empty
#define DARKSIDE_NACK_NO_SEND       0x22 // Darkside, the card doesn't respond to nack, probably a card that fixes the nack logic bug
#define DARKSIDE_TAG_CHANGED        0x23 // Darkside, there is a card switching during the running of darkside, maybe there is a signal problem, or the two cards really switched quickly
#define NESTED_TAG_IS_STATIC        0x24 // Nested, it is detected that the random number of the card response is fixed
#define NESTED_TAG_IS_HARD          0x25 // Nested, detected nonce for card response is unpredictable

#define LF_TAG_OK                   0x40 // Some operations with low frequency cards succeeded!
#define EM410X_TAG_NO_FOUND         0x41 // Unable to search for a valid EM410X label

#define STATUS_PAR_ERR              0x60 // The parameters passed by the BLE instruction are wrong, or the parameters passed by calling some functions are wrong
#define STATUS_DEVICE_MODE_ERROR    0x66 // The mode of the current device is wrong, and the corresponding API cannot be called
#define STATUS_INVALID_CMD          0x67
#define STATUS_DEVICE_SUCCESS       0x68
#define STATUS_NOT_IMPLEMENTED      0x69
#define STATUS_FLASH_WRITE_FAIL     0x70
#define STATUS_FLASH_READ_FAIL      0x71

