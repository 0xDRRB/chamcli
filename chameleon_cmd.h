#pragma once

#define CMD_GET_APP_VERSION                 1000
#define CMD_CHANGE_MODE                     1001
#define CMD_GET_DEVICE_MODE                 1002
#define CMD_SET_SLOT_ACTIVATED              1003
#define CMD_SET_SLOT_TAG_TYPE               1004
#define CMD_SET_SLOT_DATA_DEFAULT           1005
#define CMD_SET_SLOT_ENABLE                 1006

#define CMD_SET_SLOT_TAG_NICK               1007
#define CMD_GET_SLOT_TAG_NICK               1008

#define CMD_SLOT_DATA_CONFIG_SAVE           1009

#define CMD_ENTER_BOOTLOADER                1010
#define CMD_GET_DEVICE_CHIP_ID              1011
#define CMD_GET_DEVICE_ADDRESS              1012

#define CMD_SAVE_SETTINGS                   1013
#define CMD_RESET_SETTINGS                  1014
#define CMD_SET_ANIMATION_MODE              1015
#define CMD_GET_ANIMATION_MODE              1016

#define CMD_GET_GIT_VERSION                 1017

#define CMD_GET_ACTIVE_SLOT                 1018
#define CMD_GET_SLOT_INFO                   1019

#define CMD_WIPE_FDS                        1020

#define CMD_GET_ENABLED_SLOTS               1023
#define CMD_DELETE_SLOT_SENSE_TYPE          1024

#define CMD_GET_BATTERY_INFO                1025

#define CMD_GET_BUTTON_PRESS_CONFIG         1026
#define CMD_SET_BUTTON_PRESS_CONFIG         1027
#define CMD_GET_LONG_BUTTON_PRESS_CONFIG    1028
#define CMD_SET_LONG_BUTTON_PRESS_CONFIG    1029

#define CMD_SET_BLE_CONNECT_KEY_CONFIG      1030
#define CMD_GET_BLE_CONNECT_KEY_CONFIG      1031
#define CMD_DELETE_ALL_BLE_BONDS            1032
#define CMD_GET_DEVICE                      1033
#define CMD_GET_SETTINGS                    1034
#define CMD_GET_DEVICE_CAPABILITIES         1035
#define CMD_GET_BLE_PAIRING_ENABLE          1036
#define CMD_SET_BLE_PAIRING_ENABLE          1037

#define CMD_SCAN_14A_TAG                    2000
#define CMD_MF1_SUPPORT_DETECT              2001
#define CMD_MF1_NT_LEVEL_DETECT             2002
#define CMD_MF1_DARKSIDE_DETECT             2003
#define CMD_MF1_DARKSIDE_ACQUIRE            2004
#define CMD_MF1_NT_DIST_DETECT              2005
#define CMD_MF1_NESTED_ACQUIRE              2006
#define CMD_MF1_CHECK_ONE_KEY_BLOCK         2007
#define CMD_MF1_READ_ONE_BLOCK              2008
#define CMD_MF1_WRITE_ONE_BLOCK             2009

#define CMD_SCAN_EM410X_TAG                 3000
#define CMD_WRITE_EM410X_TO_T5577           3001

#define CMD_LOAD_MF1_EMU_BLOCK_DATA         4000
#define CMD_SET_MF1_ANTI_COLLISION_RES      4001
#define CMD_SET_MF1_DETECTION_ENABLE        4004
#define CMD_GET_MF1_DETECTION_COUNT         4005
#define CMD_GET_MF1_DETECTION_RESULT        4006
#define CMD_READ_MF1_EMU_BLOCK_DATA         4008
#define CMD_GET_MF1_EMULATOR_CONFIG         4009
#define CMD_GET_MF1_GEN1A_MODE              4010
#define CMD_SET_MF1_GEN1A_MODE              4011
#define CMD_GET_MF1_GEN2_MODE               4012
#define CMD_SET_MF1_GEN2_MODE               4013
#define CMD_GET_MF1_USE_FIRST_BLOCK_COLL    4014
#define CMD_SET_MF1_USE_FIRST_BLOCK_COLL    4015
#define CMD_GET_MF1_WRITE_MODE              4016
#define CMD_SET_MF1_WRITE_MODE              4017
#define CMD_GET_MF1_ANTI_COLL_DATA          4018

#define CMD_SET_EM410X_EMU_ID               5000
#define CMD_GET_EM410X_EMU_ID               5001

ssize_t serxfer(int fd, uint8_t *sdata, uint8_t *rdata, size_t len, uint16_t resp);
int cmd_get_battery(int fd, uint16_t *voltage, uint8_t *percentage);
int cmd_get_slotnick(int fd, uint8_t slot, uint8_t sensetype, char *nickname);
int cmd_get_enabledslots(int fd, uint8_t *map);
int cmd_get_activeslot(int fd);
int cmd_set_activeslot(int fd, int slotnum);
int cmd_get_slotsinfo(int fd, uint16_t *typemap);
int cmd_get_mfemulsettings(int fd, uint8_t *settings);
int cmd_set_mfemulsetwmode(int fd, uint8_t wmode);
int cmd_set_mfemulsetgen1a(int fd, uint8_t state);
int cmd_set_mfemulsetgen2(int fd, uint8_t state);
int cmd_set_mfemulsetblk0col(int fd, uint8_t state);
int cmd_set_mfemulsetdetect(int fd, uint8_t state);
int cmd_get_fwversion(int fd, uint16_t *ver);
int cmd_get_gitversion(int fd, char *strversion);
int cmd_get_blekey(int fd, char *key);
int cmd_get_bleenable(int fd);
int cmd_set_slotnick(int fd, uint8_t slot, uint8_t sensetype, char *nickname, size_t nicklen);
int cmd_save_config(int fd);
int cmd_set_slottype(int fd, uint8_t slot, uint16_t type);
int cmd_clear_slottype(int fd, uint8_t slot, uint8_t sensetype);
int cmd_loadmfblock(int fd, uint8_t offset, uint8_t *data, size_t datalen);
int cmd_set_slotenable(int fd, uint8_t slot, uint8_t sensetype, uint8_t enable);
int cmd_readmfblock(int fd, uint8_t offset, uint8_t nblock, uint8_t *data);
int cmd_set_anticoll(int fd, uint8_t *data, size_t len);
int cmd_initslot(int fd, uint8_t slot, uint16_t type);
int cmd_get_anticoll(int fd, uint8_t *data, size_t *datalen);
