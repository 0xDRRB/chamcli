#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdint.h>
#include <string.h>
#include "util.h"
#include "chameleon_status.h"
#include "chamcli.h"
#include "color.h"

void
printhelp(char *binname)
{
    printf("ChamCLI v0.1 - A simple Chameleon Ultra configuration tool.\n");
    printf("Copyright (c) 2023 - Denis Bodor\n\n");
    printf("Usage : %s [OPTIONS]\n", binname);
    printf(" -d, --device=DEVICE    use this serial device\n");
    printf(" -l, --list             list slots information\n");
    printf(" -b, --battery          display battery status\n");
    printf(" -v, --version          display firmware version\n");
    printf(" -k, --bleinfo          display BLE information\n");
    printf(" -t, --type=N           set tag type (100:EM410X, 1000:Mifare Mini, 1001:Mifare Classic 1k,\n"
			"                                      1002:Mifare Classic 2k, 1003:Mifare Classic 4k,\n"
			"                                      1100:NTAG 213, 1101:NTAG 215, 1102:NTAG 216)\n");
    printf(" -c, --cleartype        detete tag type\n");
    printf(" -a, --activate         change active slot\n");
    printf(" -i, --initslot         set data to default for emulation slot\n");
    printf(" -n, --setname=STRING   set nickname for emulation slot\n");
    printf(" -e, --enable=[0-1]     disable or enable slot\n");
	printf(" --anticoll=HEX_DATA    set anticollision data in the form of [SAK(1)][ATQA(2)][UID(4 or 7)]\n");
	printf(" --writemode=[0-3]      set MIFARE emulator write mode (%u:Normal, %u:Denied, %u:Deceive, %u:Shadow)\n",
			MFMODE_NORMAL, MFMODE_DENIED, MFMODE_DECEIVE, MFMODE_SHADOW);
	printf(" --detect=[0-1]         disable or enable MIFARE emulator Detection (mfkey32) mode\n");
	printf(" --gen1a=[0-1]          disable or enable MIFARE emulator Gen1A magic mode\n");
	printf(" --gen2=[0-1]           disable or enable MIFARE emulator Gen2 magic mode\n");
	printf(" --blk0=[0-1]           disable or enable MIFARE emulator use UID/BCC/SAK/ATQA from block 0\n");
	printf(" -r, --mfread=FILENAME  read MIFARE image from emulator memory and save it to FILENAME\n");
	printf(" -w, --mfwrite=FILENAME read MIFARE image from FILENAME and write it to emulator memory\n");
    printf(" -s, --slot=NUM         use slot NUM for operation\n");
    printf(" --lf                   set LF slot as target for operation\n");
    printf(" --hf                   set HF slot as target for operation\n");
    printf(" -h, --help             show this help\n");
}

/*
 * From : https://en.wikipedia.org/wiki/Longitudinal_redundancy_check
 *
 * lrc := 0
 * for each byte b in the buffer do
 *    lrc := (lrc + b) and 0xFF
 * lrc := (((lrc XOR 0xFF) + 1) and 0xFF)
 */
uint8_t
calclrc(uint8_t *data, size_t len)
{
	int i;
	uint8_t lrc = 0;
	for (i = 0; i < len; i++) {
		lrc = (lrc + data[i]) & 0xff;
	}
	lrc = ((lrc ^ 0xff) + 1) & 0xff;

	return(lrc);
}

size_t
hex2array(const char *line, uint8_t *data, size_t len)
{
	size_t datalen = 0;
	uint32_t temp;
	int indx = 0;
	char buf[5] = { 0 };

	while (line[indx]) {
		if (isxdigit(line[indx])) {
			buf[strlen(buf) + 1] = 0x00;
			buf[strlen(buf)] = line[indx];
		} else {
			// we have symbols other than spaces and hex
			return(0);
		}

		if (strlen(buf) >= 2) {
			sscanf(buf, "%x", &temp);
			data[datalen] = (uint8_t)(temp & 0xff);
			*buf = 0;
			datalen++;
			if (datalen > len)
				return(0);
		}

		indx++;
	}

	// no partial hex bytes
	if (strlen(buf) > 0 ) {
		return(0);
	}

	return(datalen);
}

void
printstatus(uint16_t err)
{
	switch (err) {
	case HF_TAG_OK:
		printf("HF tag operation succeeded"); break;
	case HF_TAG_NO:
		printf("HF tag no found or lost"); break;
	case HF_ERR_STAT:
		printf("HF tag status error"); break;
	case HF_ERR_CRC:
		printf("HF tag data crc error"); break;
	case HF_COLLISION:
		printf("HF tag collision"); break;
	case HF_ERR_BCC:
		printf("HF tag uid bcc error"); break;
	case MF_ERR_AUTH:
		printf("HF tag auth fail"); break;
	case HF_ERR_PARITY:
		printf("HF tag data parity error"); break;
	case DARKSIDE_CANT_FIXED_NT:
		printf("Darkside Can't select a nt(PRNG is unpredictable)"); break;
	case DARKSIDE_LUCK_AUTH_OK:
		printf("Darkside try to recover a default key"); break;
	case DARKSIDE_NACK_NO_SEND:
		printf("Darkside can't make tag response nack(enc)"); break;
	case DARKSIDE_TAG_CHANGED:
		printf("Darkside running, can't change tag"); break;
	case NESTED_TAG_IS_STATIC:
		printf("StaticNested tag, not weak nested"); break;
	case NESTED_TAG_IS_HARD:
		printf("HardNested tag, not weak nested"); break;
	case LF_TAG_OK:
		printf("LF tag operation succeeded"); break;
	case EM410X_TAG_NO_FOUND:
		printf("EM410x tag no found"); break;
	case STATUS_PAR_ERR:
		printf("API request fail, param error"); break;
	case STATUS_DEVICE_MODE_ERROR:
		printf("API request fail, device mode error"); break;
	case STATUS_INVALID_CMD:
		printf("API request fail, cmd invalid"); break;
	case STATUS_DEVICE_SUCCESS:
		printf("Device operation succeeded"); break;
	case STATUS_NOT_IMPLEMENTED:
		printf("Some api not implemented"); break;
	case STATUS_FLASH_WRITE_FAIL:
		printf("Flash write failed"); break;
	case STATUS_FLASH_READ_FAIL:
		printf("Flash read failed"); break;
	default:
		printf("Unknown error");
	}
	printf("\n");
}

void
printtagtype(uint16_t type)
{
	switch (type) {
	case TYPE_EM410X:
		printf("EM410X"); break;
	case TYPE_MIFARE_Mini:
		printf("Mifare Mini"); break;
	case TYPE_MIFARE_1024:
		printf("Mifare Classic 1k"); break;
	case TYPE_MIFARE_2048:
		printf("Mifare Classic 2k"); break;
	case TYPE_MIFARE_4096:
		printf("Mifare Classic 4k"); break;
	case TYPE_NTAG_213:
		printf("NTAG 213"); break;
	case TYPE_NTAG_215:
		printf("NTAG 215"); break;
	case TYPE_NTAG_216:
		printf("NTAG 216"); break;
	default:
		printf("Unknown");
	}
}

void
printmfemulsettings(uint8_t *settings)
{
	printf("Mifare Classic emulator settings:\n");
	printf("  Detection (mfkey32) mode:          %s" RESET "\n", settings[0] ? BOLDGREEN "enabled" : RED "disabled");
	printf("  Gen1A magic mode:                  %s" RESET "\n", settings[1] ? BOLDGREEN "enabled" : RED "disabled");
	printf("  Gen2 magic mode:                   %s" RESET "\n", settings[2] ? BOLDGREEN "enabled" : RED "disabled");
	printf("  Use UID/BCC/SAK/ATQA from block 0: %s" RESET "\n", settings[3] ? BOLDGREEN "enabled" : RED "disabled");

	printf("  Write mode:                        ");
	switch(settings[4]) {
	case MFMODE_NORMAL:
		printf(GREEN "Normal" RESET "\n");
		break;
	case MFMODE_DENIED:
		printf(RED "Denied" RESET " (Send NACK to write attempts)\n");
		break;
	case MFMODE_DECEIVE:
		printf(YELLOW "Deceive" RESET " (Acknowledge writes, but don't do it)\n");
		break;
	case MFMODE_SHADOW:
		printf(CYAN "Shadow" RESET " (store data to RAM, so data disappears as soon as the tag leaves the EM field)\n");
		break;
	}
}

void
printmfemulsettingsline(uint8_t *settings)
{
	printf("MIFARE emulator settings:  write:");
	switch(settings[4]) {
	case MFMODE_NORMAL:
		printf(GREEN "Normal" RESET);
		break;
	case MFMODE_DENIED:
		printf(RED "Denied" RESET);
		break;
	case MFMODE_DECEIVE:
		printf(YELLOW "Deceive" RESET);
		break;
	case MFMODE_SHADOW:
		printf(CYAN "Shadow" RESET);
		break;
	}

	printf("  detect:%s" RESET, settings[0] ? BOLDGREEN "ON" : RED "OFF");
	printf("  Gen1A:%s" RESET, settings[1] ? BOLDGREEN "ON" : RED "OFF");
	printf("  Gen2:%s" RESET, settings[2] ? BOLDGREEN "ON" : RED "OFF");
	printf("  block0anticol:%s" RESET, settings[3] ? BOLDGREEN "ON" : RED "OFF");

}

void
printanticolldata(uint8_t *data, size_t len)
{
	int i;

	// uidlen|uid[uidlen]|atqa[2]|sak|atslen|ats[atslen]
	printf("UID(%u):", data[0]);
	for (i = 0; i < data[0]; i++) {
		printf("%02X", data[1 + i]);
	}
	printf("  ATQA:%02X%02X", data[1 + data[0]], data[1 + data[0] + 1]);
	printf("  SAK:%02X", data[1 + data[0] + 2]);
	printf("  ATS(%u):", data[1 + data[0] + 3]);
	for (i = 0; i < data[1 + data[0] + 3]; i++) {
		printf("%02X", data[1 + data[0] + 4 + i]);
	}

}
